import redis
import logging

from settings_local import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD

logger = logging.getLogger(__name__)

class RedisDataClass:

    def __init__(self, key_name, msg):
        self.host = REDIS_HOST
        self.port = REDIS_PORT
        self.password = REDIS_PASSWORD
        self.key_name = key_name
        self.msg = msg


    def get_redis_connection(self):

        try:

            redis_conn = redis.StrictRedis(host=self.host, port=self.port, password=self.password, decode_responses=True)
        except:
            logger.error("Fatal error in get_redis_connection!", exc_info=True)

            return redis_conn


    def set_redis_object(self):

        try:

            r = self.get_redis_connection()
            r.set(self.key_name, self.msg)
        except Exception:
            logger.error("Fatal error in set_redis_object!", exc_info=True)


    def get_redis_object(self):
        msg = "Empty value!"

        try:

            r = self.get_redis_connection()
            msg = r.get(self.key_name)
        except Exception:
            logger.error("Fatal error in get_redis_object!", exc_info=True)

        return msg


