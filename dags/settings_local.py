"""Parameters for redis connection """
REDIS_HOST = "localhost"
REDIS_PORT =6379
REDIS_PASSWORD = ""

# Other parameters
BUCKET_NAME = ""
connection_to_s3_name = "my_S3_conn"
file_name_in_S3 = "test_result"
http_connection_id = "user_api"
