# Instructions

1. Create a new git project

2. Create a file named ".gitignore" and add all the untracked files into it.

3. Initialization
```buildoutcfg
git init
git remote add origin "my_repository_url"
```

4. Push the first commit

```buildoutcfg
git status
git add .gitignore
git add README.md
git status
git commit -m "Initial commit"

```

5. Create a dev branch and switch to it, it will contain all the history 
of the projects
   
```buildoutcfg
git checkout -b dev
```
6. Push the committed changes into the branch "dev"

```buildoutcfg
git push origin dev
```
7. We can create a merge request to the main branch or we can continue 
adding more file into our project.
   
   
8. Create the project structure (folders+files) and commit the changes.


10. Install the needed packages and Initialize the project (eg. Apache-airflow project)

```buildoutcfg
pip 
```


9. Some useful tips:

* Show the history of the commits
  
```buildoutcfg
git log
```
* If you want to delete a specific commit before pushing it.
```buildoutcfg
git reset "commit_id" 
```
